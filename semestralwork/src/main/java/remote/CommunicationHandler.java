package remote;

import model.Clock;
import model.ElectionMessage;
import model.Address;
import model.TopologyInfo;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
/*
    Rozhrani vzdaleneho objektu.
 */
public interface CommunicationHandler extends Remote, Chat {

    public TopologyInfo join( Address me, Clock clock) throws RemoteException, MalformedURLException, NotBoundException;
    public void receiveMessage( ElectionMessage electionMessage,Clock clock ) throws RemoteException;

    public void changePrev( Address me, Clock clock) throws RemoteException;
    public void changeNext( Address me, Clock clock) throws RemoteException;
    public void changeNnext( Address me, Clock clock) throws RemoteException;

    public Address getLeaderLink( Clock clock) throws RemoteException;
    public Address getNext( Clock clock) throws RemoteException;

    void failedNode( Address idAddress, Clock clock ) throws RemoteException;

    void removeMember( Address address, Clock clock ) throws RemoteException;
    void addMember( Address address, String chatName,Clock clock ) throws RemoteException;
}

package remote;

import model.ChatMessage;
import model.Clock;

import java.rmi.Remote;
import java.rmi.RemoteException;
/*
    Rozhrani chatu.
 */
public interface Chat extends Remote {

    public void send( ChatMessage msg, Clock clock) throws RemoteException;
    public void receive(ChatMessage msg, Clock clock) throws RemoteException;
}

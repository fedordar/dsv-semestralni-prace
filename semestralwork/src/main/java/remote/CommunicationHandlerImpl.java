package remote;

import model.*;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/*
    Vzdalena sluzba. Upravuje topologie, provadi volby a odesila zpravy.
 */
public class CommunicationHandlerImpl implements CommunicationHandler {

    private static DsvLogger LOG = DsvLogger.getInstance();
    private ClusterNode myNode;
    private boolean isRepairing = false;
    private boolean isInitiator = false;

    public CommunicationHandlerImpl(ClusterNode myNode) throws RemoteException {
        this.myNode = myNode;
    }
    private boolean isLeader() throws RemoteException {
        synchronized (myNode) {
            return myNode.isLeader();
        }
    }

    @Override public void failedNode( Address idAddress, Clock clock ) throws RemoteException {
        if(isRepairing)
            return;
        setRepairing( true );
        LOG.warn( clock, "Node missing with address"+ idAddress );
//            if failed node is my next, im repairing system topology
        if(myNode.getTopology().getNext().compareTo( idAddress ) == 0){
            repairRing();
        }else {
            setRepairing( false );
            myNode.getRemoteService( myNode.getTopology().getNext() ).failedNode( idAddress, LOG.getClock());
            return;
        }
        setRepairing( false );
        LOG.info( "Finish repair" );
        LOG.info( "failedNode: new topology "+ myNode.getTopology() );
        if(!isLeaderAlive()){
            LOG.info( "Start elections" );
            setInitiator(true);
            startElection();
        }else myNode.getRemoteService( getLeaderLink( LOG.getClock() ) ).removeMember(idAddress, LOG.getClock());
    }

    private void setInitiator( boolean b ) {
        this.isInitiator = b;
    }

    private void startElection() throws RemoteException {
        myNode.getRemoteService( myNode.getTopology().getNext() ).receiveMessage( new ElectionMessage(MessageType.ONE,myNode.getCi() ), LOG.getClock());
    }

    @Override public void receiveMessage( ElectionMessage electionMessage, Clock clock ) throws RemoteException {
        LOG.info(clock,"ReceiveMessage: election message " + electionMessage.toString() );
        switch ( electionMessage.getMessageType()){
            case ONE:
                processMessageOne(electionMessage);
                break;
            case TWO:
                processMessageTwo(electionMessage);
                break;
            case SMALL:
                processMessageSmall(electionMessage );
                break;
            case INITIATOR:
                if(myNode.getState().equals( NodeState.ACTIVE )){
                    setInitiator( true );
                    myNode.getRemoteService( myNode.getTopology().getNext() ).receiveMessage( new ElectionMessage( MessageType.ONE , myNode.getCi()), LOG.getClock() );
                }else myNode.getRemoteService( myNode.getTopology().getNext() ).receiveMessage( new ElectionMessage( MessageType.INITIATOR, null ), LOG.getClock() );
                break;
        }
    }

    private void processMessageSmall( ElectionMessage electionMessage ) throws RemoteException {
        LOG.info("processMessageSmall: leader is " + electionMessage.getLeader() );
        myNode.getTopology().setLeader( electionMessage.getLeader() );
        setInitiator( false );
        myNode.setState( NodeState.ACTIVE );
        myNode.setCi( myNode.getId() );
        myNode.setAcn( 0L );
        if(!myNode.getAddress().equals( electionMessage.getLeader() )) {
            myNode.getRemoteService( myNode.getTopology().getNext() ).receiveMessage( electionMessage, LOG.getClock() );
            myNode.getRemoteService( myNode.getTopology().getLeader() ).addMember(myNode.getAddress(),myNode.chatName, LOG.getClock() );
        }
        else {
            myNode.getTopology().setLeaderMemberInfo( new LeaderMemberInfo() );
            addMember(myNode.getAddress(),myNode.chatName, LOG.getClock() );
        }
    }

    @Override public void addMember( Address address, String chatName,Clock clock )  throws RemoteException{
        LOG.info("addMember: new member in table " + address+" with name "+chatName );
        final TopologyInfo topology = myNode.getTopology();
        synchronized (topology) {
            topology.getLeaderMemberInfo().addMember( address, chatName );
            LOG.info("addMember:table after adding: "+ topology.getLeaderMemberInfo().toString()  );
        }
    }
    @Override public void removeMember( Address address, Clock clock )  throws RemoteException{
        LOG.info("removeMember: member from table " + address);
        final TopologyInfo topology = myNode.getTopology();
        synchronized (topology) {
            topology.getLeaderMemberInfo().removeMember( address );
            LOG.info("removeMember:table after removing: "+ topology.getLeaderMemberInfo().toString()  );
        }
    }


    private void processMessageOne( ElectionMessage electionMessage ) throws RemoteException {
        switch ( myNode.getState() ){
        case ACTIVE:
            //  jsem jediny Active (protoze id zpravy = muj id)
            if (electionMessage.getId().equals( myNode.getCi() )) {
                myNode.getRemoteService(myNode.getTopology().getNext()).receiveMessage(new ElectionMessage( MessageType.SMALL, myNode.getCi(), myNode.getAddress() ), LOG.getClock() );
                return;
            }
            myNode.setAcn( electionMessage.getId() );
            // pokud jsem iniciator, tak skoncim prvni round a zacnu druhy
            if(isInitiator)
                myNode.getRemoteService( myNode.getTopology().getNext() ).receiveMessage( new ElectionMessage( MessageType.TWO, myNode.getAcn() ), LOG.getClock() );
            else myNode.getRemoteService( myNode.getTopology().getNext() ).receiveMessage( new ElectionMessage( MessageType.ONE, myNode.getCi() ), LOG.getClock() );
            break;
        case PASSIVE:
            myNode.getRemoteService( myNode.getTopology().getNext() ).receiveMessage( electionMessage, LOG.getClock() );
            break;
        }
    }

    private void processMessageTwo( ElectionMessage electionMessage ) throws RemoteException {
        switch ( myNode.getState() ){
        case ACTIVE:
            // pokud nejsem iniciator, odeslu dalsi zpravu
            // vyhodnoceni zpravy
            if( myNode.getAcn() < myNode.getCi() && myNode.getAcn() < electionMessage.getId())
                myNode.setCi( myNode.getAcn() );
            else myNode.setState( NodeState.PASSIVE );
            // kdyz druhy round skonci a iniciator je passive, iniciatorem se stane dalsi active uzel//
            if(isInitiator) {
                if ( myNode.getState().equals( NodeState.PASSIVE ) )
                    myNode.getRemoteService( myNode.getTopology().getNext() ).receiveMessage( new ElectionMessage( MessageType.INITIATOR, null ), LOG.getClock() );
                else
                    myNode.getRemoteService( myNode.getTopology().getNext() ).receiveMessage( new ElectionMessage( MessageType.ONE, myNode.getCi() ), LOG.getClock() );
            }else myNode.getRemoteService( myNode.getTopology().getNext() ).receiveMessage( new ElectionMessage( MessageType.TWO, myNode.getAcn() ), LOG.getClock() );
            break;
        case PASSIVE:
            myNode.getRemoteService( myNode.getTopology().getNext() ).receiveMessage( electionMessage, LOG.getClock() );
            break;
        }
    }



    private boolean isLeaderAlive() {
        Address leader = myNode.getTopology().getLeader();
        try {
            Registry registry = LocateRegistry.getRegistry(leader.getHost(),leader.getPort());
            registry.lookup( ClusterNode.name );
        }catch ( Exception e ){
            LOG.info("Leader died"  );
            return false;
        }
        return true;
    }

    @Override public TopologyInfo join( Address newNode, Clock clock ) throws RemoteException {
        LOG.info(clock,"Join: new node = " + newNode.toString() );
        if(isRepairing || isInitiator ){
            LOG.info("Join while repairing or elections. Throw exception.");
            throw new RemoteException();
        }
        final TopologyInfo topology = myNode.getTopology();
        // Jsem první uzel, připojuji se sám na sebe
        synchronized (topology) {
            if (newNode.compareTo( myNode.getAddress() ) == 0) {
                topology.setLeader(newNode);
                topology.setLeaderMemberInfo( new LeaderMemberInfo() );
                topology.getLeaderMemberInfo().addMember( myNode.getAddress(), myNode.chatName);
                topology.setNnext(newNode);
                topology.setNext(newNode);
                topology.setPrev(newNode);
                return topology;
            } else {
                Set<Address> participants= ConcurrentHashMap.newKeySet();
                TopologyInfo tmpNeighbours = new TopologyInfo(topology.getNext(),
                        topology.getNnext(),
                        myNode.getAddress(),
                        topology.getLeader() );

                Address oldNext = new Address(topology.getNext());
                Address oldPrev = new Address(topology.getPrev());

                // to my next send msg ChPrev to addr
                myNode.getRemoteService( oldNext ).changePrev( newNode ,LOG.getClock());
                // to my prev send msg ChNNext addr
                myNode.getRemoteService( oldPrev ).changeNnext(newNode,LOG.getClock());
                tmpNeighbours.setNnext(topology.getNnext());
                topology.setNext( newNode );
                topology.setNnext( oldNext );

                LOG.info("Join: my topology after join new node " + topology);
                return tmpNeighbours;
            }
        }
    }

    @Override public void changePrev( Address me, Clock clock ) throws RemoteException {
        LOG.info( clock, "changePrev from:"+myNode.getTopology().getPrev()+" to "+me.toString());
        final TopologyInfo topology = myNode.getTopology();
        synchronized (topology) {
            topology.setPrev( me );
        }
        LOG.info( "changePrev: new topology "+topology.toString());
    }

    @Override public void changeNext( Address me, Clock clock ) throws RemoteException {
        LOG.info( clock, "changeNext from:"+myNode.getTopology().getNext()+" to "+me.toString());
        final TopologyInfo topology = myNode.getTopology();
        synchronized (topology) {
            topology.setNext( me );
        }
        LOG.info( "changeNext: new topology "+topology.toString());
    }

    @Override public void changeNnext( Address me, Clock clock ) throws RemoteException {
        LOG.info( clock, "changeNnext from:"+myNode.getTopology().getNnext()+" to "+me.toString());
        final TopologyInfo topology = myNode.getTopology();
        synchronized (topology) {
            topology.setNnext( me );
        }
        LOG.info( "changeNnext: new topology "+topology.toString());
    }


    @Override public Address getNext(Clock clock) throws RemoteException {
        LOG.info( clock, "getNext node:"+myNode.getTopology().getNext());
        final TopologyInfo topology = myNode.getTopology();
        synchronized (topology) {
            return topology.getNext();
        }
    }

    private void repairRing() throws RemoteException {
        TopologyInfo topology = myNode.getTopology();
        synchronized (topology) {
//            change my next
            myNode.getRemoteService( myNode.getAddress() ).changeNext( topology.getNnext(),LOG.getClock() );
//            topology.setNext( topology.getNnext() );
            CommunicationHandler handlerMyNext = myNode.getRemoteService( topology.getNext() );
//            change prev of my next to me
            handlerMyNext.changePrev( myNode.getAddress() ,LOG.getClock());
            Address nextOfMyNext = handlerMyNext.getNext(LOG.getClock());
//            change my nextnext
            myNode.getRemoteService( myNode.getAddress() ).changeNnext( nextOfMyNext,LOG.getClock() );
//            topology.setNnext( nextOfMyNext );
//            change nextnext of my prev
            myNode.getRemoteService( topology.getPrev() ).changeNnext( topology.getNext(),LOG.getClock() );
        }
    }

    @Override public Address getLeaderLink(Clock clock) {
        LOG.info( clock, "getLeaderLink node:"+myNode.getTopology().getLeader());
        final TopologyInfo topology = myNode.getTopology();
        synchronized (topology) {
            return topology.getLeader();
        }    }

    public void setRepairing( boolean repairing ){
        this.isRepairing = repairing;
    }

    @Override
    public void send(ChatMessage msg, Clock clock) throws RemoteException {
        LOG.info( clock, "send chat message"+msg.toString());
        if(isLeader()) {
            try {
                Map<String , Address> info = myNode.getTopology().getLeaderMemberInfo().getMemberInfo();
                String name = msg.getName();
                Address in = info.get( name );
                myNode.getRemoteService( in).receive( msg, LOG.getClock() );
            }
            catch ( Exception e ) {
                LOG.info( "#send:" + e );
                throw new RemoteException( e.getLocalizedMessage() );
            }
        } else myNode.getRemoteService( getLeaderLink(LOG.getClock()) ).send( msg,LOG.getClock());
    }

    @Override
    public void receive(ChatMessage msg, Clock clock) throws RemoteException {
        LOG.info( clock, "receive chat message"+msg.toString());
        System.out.println(msg.getFrom()+":"+msg.getText());
    }

}

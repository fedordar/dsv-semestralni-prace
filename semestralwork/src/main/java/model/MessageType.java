package model;

public enum MessageType {
    ONE,
    TWO,
    SMALL,
    INITIATOR;
}

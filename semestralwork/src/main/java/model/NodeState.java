package model;

public enum NodeState {
    ACTIVE,
    PASSIVE;
}

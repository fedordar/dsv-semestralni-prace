package model;

import java.io.Serializable;

/*
    Topologie systemu, ktere zna kazdy uzel. Prev, next, nnezt je nutne pro vytvoreni a uchovani spravnosti topologie. 
 */
public class TopologyInfo implements Serializable {
    private Address prev;
    private Address next;
    private Address nnext;
    private Address leader;
    private LeaderMemberInfo leaderMemberInfo = null;

    public TopologyInfo( Address next, Address nnext, Address prev, Address leader ) {
        this.prev = prev;
        this.next = next;
        this.nnext = nnext;
        this.leader = leader;
    }

    public TopologyInfo( Address prev, Address next, Address nnext, Address leader, LeaderMemberInfo leaderMemberInfo ) {
        this.prev = prev;
        this.next = next;
        this.nnext = nnext;
        this.leader = leader;
        this.leaderMemberInfo = leaderMemberInfo;
    }

    public TopologyInfo() {
    }

    public Address getPrev() {
        return prev;
    }

    public void setPrev( Address prev ) {
        this.prev = prev;
    }

    public Address getNext() {
        return next;
    }

    public void setNext( Address next ) {
        this.next = next;
    }

    public Address getNnext() {
        return nnext;
    }

    public void setNnext( Address nnext ) {
        this.nnext = nnext;
    }

    public Address getLeader() {
        return leader;
    }

    public void setLeader( Address leader ) {
        this.leader = leader;
    }

    public LeaderMemberInfo getLeaderMemberInfo() {
        return leaderMemberInfo;
    }

    public void setLeaderMemberInfo( LeaderMemberInfo leaderMemberInfo ) {
        this.leaderMemberInfo = leaderMemberInfo;
    }

    @Override public String toString() {
        return "TopologyInfo{" +
                "prev=" + prev +
                ", next=" + next +
                ", nnext=" + nnext +
                ", leader=" + leader +
                '}';
    }
}

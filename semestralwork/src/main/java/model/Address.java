package model;

import java.io.Serializable;
import java.util.Objects;

/*
    Trida identifikator pro kazdy uzel. Trojice id, host a port je unikatni v ramci systemu. Id je generovano automaticky.
 */
public class Address implements Serializable, Comparable<Address> {
    private String host;
    private Integer port;

    public Address( String myAddress ) {
        this.host = myAddress.split(":")[0];
        this.port = Integer.parseInt(myAddress.split(":")[1]);
    }

    public Address( Address idAddress ) {
        this( idAddress.getHost(), idAddress.getPort());
    }


    public Address( String host, Integer port ) {
        this.host = host;
        this.port = port;
    }
    public String getHost() {
        return host;
    }

    public void setHost( String host ) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    @Override public String toString() {
        return "host/port uzlu:"+host+"/"+port;
    }

    @Override public boolean equals( Object o ) {
        if ( this == o )
            return true;
        if ( o == null || getClass() != o.getClass() )
            return false;
        Address idAddress = ( Address ) o;
        return Objects.equals( host, idAddress.host ) && Objects.equals( port, idAddress.port );
    }

    @Override public int hashCode() {
        return Objects.hash( host, port );
    }

    public void setPort( Integer port ) {
        this.port = port;
    }

    @Override public int compareTo( Address o ) {
        if(port.equals( o.getPort() )&& host.equals( o.getHost()))
            return 0;
        return -1;
    }
}

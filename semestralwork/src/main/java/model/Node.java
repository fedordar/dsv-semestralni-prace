package model;

import java.io.Serializable;

public interface Node extends Serializable {

    public boolean isLeader();
    public Address getAddress();

}

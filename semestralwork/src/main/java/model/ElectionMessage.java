package model;

import java.io.Serializable;

public class ElectionMessage implements Serializable {
    private MessageType messageType;
    private Long id;
    private Address leader;

    public ElectionMessage( MessageType messageType, Long id ) {
        this.messageType = messageType;
        this.id = id;
        leader = null;
    }

    public ElectionMessage( MessageType messageType, Long id, Address leader ) {
        this.messageType = messageType;
        this.id = id;
        this.leader = leader;
    }

    @Override public String toString() {
        return "ElectionMessage{" +
                "messageType=" + messageType +
                "and id=" + id +
                '}';
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public Long getId() {
        return id;
    }

    public Address getLeader() {
        return leader;
    }
}

package model;

import remote.CommunicationHandler;
import remote.CommunicationHandlerImpl;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Random;

/*
    Uzel v systemu. Ma identifikator adresu, nazev vzdalene sluzby(vzdy je stejne u vsech uzlu).
    CommunicationHandler je object, ktery je vystaven ven jako vzdaleny objekt Java RMI. Pomoci tohoto handleru probiha komunikace mezi uzly.
 */
public class ClusterNode implements Node{
    private Address myIdAddress;
    public final static String name = "DSV_REMOTE_SERVICE_CHAT";
    public final String chatName;
    private CommunicationHandler communicationHandler;
    private TopologyInfo topology;
    private NodeState state;
    private final Long id;
    private Long ci;
    private Long acn = 0L;

    private static DsvLogger LOG = DsvLogger.getInstance();

    public ClusterNode( Address idAddress, String name ) throws RemoteException {
        this.myIdAddress = idAddress;
        this.state = NodeState.ACTIVE;
        this.id = generateId();
        this.ci = id;
        this.chatName = name;
        bind();
        LOG.info("Node id: "+id+", host: "+ getAddress().getHost()+", port:"+ getAddress().getPort() );
    }
    public Long getId() {
        return id;
    }

    public Long getCi() {
        return ci;
    }

    public void setCi( Long ci ) {
        this.ci = ci;
    }

    public Long getAcn() {
        return acn;
    }

    public void setAcn( Long acn ) {
        this.acn = acn;
    }

    public long generateId() {

        String[] ipAddressInArray = myIdAddress.getHost().split( "\\." );
        long result= 0;
        for ( String s : ipAddressInArray ) {
            int ip = Integer.parseInt( s );
            result = result * 1000;
            result += ip;
        }
        return myIdAddress.getPort() * 1000000000000L + result;

    }

    public NodeState getState() {
        return state;
    }

    public void setState( NodeState state ) {
        this.state = state;
    }

    public TopologyInfo getTopology() {
        return topology == null ? new TopologyInfo() : topology;
    }

/*
    Registrace objektu jako vzdalene sluzky v registru (stub).
 */
    public void bind() throws RemoteException {
        LOG.info("#bind: listening on  host: " + myIdAddress.getHost()+", port: "+ myIdAddress.getPort()+", id: "+id);
        System.setProperty("java.rmi.server.hostname", myIdAddress.getHost());
        try {
            final Registry registry = LocateRegistry.createRegistry( myIdAddress.getPort());
            communicationHandler = new CommunicationHandlerImpl(this);
            CommunicationHandler skeleton =
                    (CommunicationHandler) UnicastRemoteObject.exportObject(communicationHandler, 0);
            registry.rebind(ClusterNode.name, skeleton );
        }catch ( RemoteException e ){
            LOG.info( "failed in bind with error "+e.getMessage() );
            throw new RemoteException();
        }

    }
/*
    Odhlaseni sluzky z registru.
 */
    public void unbind() throws RemoteException, NotBoundException {
        final Registry registry = LocateRegistry.createRegistry( myIdAddress.getPort());
        registry.unbind(ClusterNode.name);
    }
/*
    Ziskavani vzdaleneho objektu (skeleton).
 */
    public CommunicationHandler getRemoteService( Address idAddress ) throws RemoteException {
        if ( myIdAddress.compareTo( idAddress )==0) {
            return communicationHandler;
        }
        try {
            Registry registry = LocateRegistry.getRegistry( idAddress.getHost(), idAddress.getPort());
            return ( CommunicationHandler ) registry.lookup(ClusterNode.name);

        }catch ( NotBoundException | RemoteException e ){
            LOG.warn( "Node with address "+ idAddress +" is missing" );
            communicationHandler.failedNode( idAddress, LOG.getClock() );
            throw new RemoteException("");
        }

    }

    /*
     * Připojení k uzlu, nastavení nových sousedů.
     */
    public void connectTo( Address idAddress ) throws MalformedURLException, RemoteException, NotBoundException {
        LOG.info("#connectTo: connect to node  " + idAddress.getHost()+" "+ idAddress.getPort());
        final CommunicationHandler handler = getRemoteService( idAddress );
        final TopologyInfo topology = handler.join( myIdAddress,LOG.getClock() );
        this.topology = topology;
        getRemoteService( topology.getLeader() ).addMember( myIdAddress,chatName ,LOG.getClock());
        LOG.info( "connectTo: my topology"+topology.toString());

    }
    /*
    Vyvola se pri odesilani nove zpravy chatu.
     */
    public void sendMessage(final String msg,String name ) throws RemoteException {
        communicationHandler.send( new ChatMessage( myIdAddress,msg, name),LOG.getClock() );
    }

    public void onClose() {
        try {
            LOG.info("#disconnect: disconnection " + myIdAddress );
            getRemoteService( topology.getPrev() ).failedNode( myIdAddress, LOG.getClock());
        } catch (RemoteException re) {
            LOG.warn( re.getMessage());
        }
    }


    @Override public boolean isLeader() {
        return myIdAddress.compareTo( topology.getLeader() ) == 0;
    }

    @Override public Address getAddress() {
        return myIdAddress;
    }
}

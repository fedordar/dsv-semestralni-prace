package model;

import java.io.Serializable;

public class Clock implements Serializable {
    private int time = 0;

    public int getTimeAndIncrement() {
        return ++time;
    }

    public int getTime() {
        return time;
    }

    int addTime(Clock anotherTime){
        time = Math.max( time,anotherTime.getTime());
        return ++time;
    }
}

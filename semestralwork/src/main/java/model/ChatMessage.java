package model;

import java.io.Serializable;
/*
    Urcuje jednotlivu zpravu v ramci chatu.
 */
public class ChatMessage implements Serializable {

    private static final long serialVersionUID = -8631385724542852341L;

    private Address from;
    private String name;
    private String text;

    public ChatMessage ( Address me, String text, String name) {
        this.from=me;
        this.text=text;
        this.name = name;
    }

    public Address getFrom() {
        return from;
    }

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        final StringBuilder sb=new StringBuilder();
        sb.append(from).append(": ");
        sb.append(text);
        return sb.toString();
    }

}

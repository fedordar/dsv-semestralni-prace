package model;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class LeaderMemberInfo {
    private Map<String,Address> memberInfo = Collections.synchronizedMap(new HashMap<String, Address>());

    public void addMember(Address address, String name){
        memberInfo.put( name,address );
    }
    public void removeMember(Address address){
        for( Map.Entry<String, Address> entry: memberInfo.entrySet() ){
            if(entry.getValue().equals( address ))
                memberInfo.remove( entry.getKey() );
        }
    }

    public Map<String,Address> getMemberInfo() {
        return memberInfo;
    }

    @Override public String toString() {
        return "LeaderMemberInfo{" +
                "memberInfo=" + memberInfo.toString() +
                '}';
    }
}

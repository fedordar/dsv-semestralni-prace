package model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DsvLogger{
    private static final Logger LOG = LoggerFactory.getLogger( DsvLogger.class.getName());
    private static DsvLogger INSTANCE = null;
    private Clock clock;

    private DsvLogger() {
        clock = new Clock();
    }

    public Clock getClock() {
        return clock;
    }

    public static DsvLogger getInstance()
    {
        if (INSTANCE == null)
            INSTANCE = new DsvLogger();

        return INSTANCE;
    }
    public void info(Clock anotherClock, String text){
        LOG.info( "Time:"+ clock.addTime( anotherClock )+". "+text );
    }
    public void info(String text){
        LOG.info( "Time:"+clock.getTimeAndIncrement()+". "+text );
    }
    public void warn(Clock anotherClock, String text){
        LOG.warn( "Time:"+clock.addTime( anotherClock )+". "+text );
    }
    public void warn(String text){
        LOG.warn( "Time:"+clock.getTimeAndIncrement()+". "+text );
    }
    public void error(Clock anotherClock, String text){
        LOG.error( "Time:"+clock.addTime( anotherClock )+". "+text );
    }
    public void error(String text) {
        LOG.error( "Time:" + clock.getTimeAndIncrement() + ". " + text );
    }
}

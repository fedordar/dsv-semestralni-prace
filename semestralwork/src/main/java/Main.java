import model.DsvLogger;
import model.Address;
import model.ClusterNode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.UnknownHostException;

public class Main {

    private static DsvLogger LOG = DsvLogger.getInstance();

    public static void main(final String[] args) throws UnknownHostException {
        LOG.info("#main: starting");
        if (args.length>3) {
            System.out.println("Error: too many arguments\n");
            System.out.println("Usage examples:");
            System.out.println(" 127.0.1.1:2010");
            System.out.println(" 127.0.1.1:2011 127.0.1.1:2010 Lukas");
            return;
        }

        String myAddress="127.0.1.1:2010";
        String remoteAddress="127.0.1.1:2010";
        String name="Lukas";

        if (args.length>=1) {
            myAddress=args[0];
        }
        if (args.length>=2) {
            remoteAddress=args[1];
        }
        if (args.length>=2) {
            name=args[2];
        }

        final ClusterNode clusterNode;
        try {

            clusterNode = new ClusterNode( new Address(myAddress ),name );
            LOG.info("#main: my address = " + myAddress);
            clusterNode.connectTo(new Address( remoteAddress ));
        } catch (Exception e) {
            LOG.error("#main: Nastaveni nebo propojeni k systemu selhalo. Zkuste jeste za chvili.");
            System.exit(0);
            return;
        }

        LOG.info("#info: Initialization was complete");
        final BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String line;
            try {
                line = br.readLine();

                if ("!quit".equals(line)) {
                    LOG.info("#main: exiting");
                    clusterNode.onClose();
                    System.exit(0);
                }
                if (line != null) {
                    String[] msg =  line.split( ":" );
                    if(msg.length==2)
                        clusterNode.sendMessage(msg[1],msg[0]);
                }
            } catch (IOException e) {
            }
        }
    }

}

## Zadani  
LE - Peterson - Java RMI - chat

## Leader Election  
První uzel v systému je leader. Při zapojení do systému uzel převezme existujícího leadera. Volby nastává jen když umře leader. Volba vůdce se probíhá pomocí algoritmu Bully až skončí oprava topologie.  

## Popis algoritmu Peterson  
Každý uzel v algoritmu Petersona může být v aktivním nebo pasivním stavu.Když algoritmus začne
práce, všechny uzly budou v aktivním režimu.  Každý uzel má virtuální ID značku (CI), která je
iniciováno na ID uzlu na začátku provádění. V aktivním režimu každý uzel odešle aktuální virtuální identifikátor dalšímu uzlu a čeká, dokud 
obdrží první zprávu od sousedního uzlu. Poté porovnává přijatou zprávu s
svým aktuálním virtuálním identifikátorem. Pokud se identifikátor shoduje, prohlásí se za vůdce. Jinak
každý uzel předá druhou zprávu, ve které předá ID (ACN) první zprávy, dalšímu sousednímu uzlu v kroužku a čeká na přijetí
další zprávy. Když každý uzel obdrží druhou zprávu, porovnává první přijatou zprávu (ACN)
s novým získaným (currMesId) a jeho virtuálním identifikátorem(CI). Pokud id první přijaté zprávy je menší
identifikátor druhé zprávy a virtuální identifikátor (if acn < ci and acn < currMesId), aktualizuje své virtuální ID (CI) na ID první přijaté
zprávy(ACN). V opačném případě přejde do Passive režimu. V režimu uzel jednoduše přenáší přijaté  zprávy  k dalšímu sousednímu uzlu v kruhu bez provedení dalšího zpracování přijatých
zpráv.
![Algoritmus](Peterson.jpg)

## Topologie  
V rámci tohoto projektu každý process je jednotlivě identifikován ip adresou, portem a číselným id. Každý proces zná svou vlastní identifikaci, a to pro každý jiný proces.
Tato informace (s výjimkou id) je předána pomocí argumentů funkce Main nebo je nastavena automaticky. Taky v argimentech je informace o uzle, na který se má připojit. Uzly v systému jsou zařazené do kruhové topologie. Každý uzel zná:
* Next - následující uzel
* Nnext - následující uzel následujícího
* Prev - předešlý
* Leader - koordinátor

Pokud je uzel první, připojuje se sám na sebe, všechny ukazatele ukazují na stejný uzel. Když probíhá oprava topologie nebo volby, uzel se nemůže připojit.

### Oprava  
Chybu v topoligie zjistíme ve chvíli, když nějaký uzel požádá o získání vzdáleného objektu uzlu, který se vypnul. Opravu topologie se zabývá živý uzel, jehož následující soused je mtrvy. Uzel, který se dozvěděl o poruše, odesílá zprávu následujícímu uzlů. Skončí to až narazíme na uzel, který má mrtvý následující uzel.
Oprava je možná jen v případě výpadku jednoho uzlu. 

### Logický čas
Lamportovy hodiny
Je důležité pořadí, nikoli přesný čas; nekomunikující procesy nemusí být synchronizovány.

Kdykoli proces zaznamená důležitou událost (generování zprávy), inkrementuje timestamp
Ke každé poslané zprávě přidá timestamp
Když proces p přijme zprávu m, aktualizuje si svůj timestamp: TS(p) = max(TS(p), TS(m))+1

### Ukazkovy priklad  

![Algoritmus](Process.png)
